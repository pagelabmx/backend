# django-rest-app
Django REST framework


* ### 2. Instalación
    * `$ git clone https://perezatanaciod@bitbucket.org/pagelabmx/backend.git`
    * cd backend


# Crear en entorno virtual
    * virtualenv -p python3 env
    * source env/bin/activate


# Instalación de Django y Django REST framework dentro del virtualenv
    * pip install django
    * pip install djangorestframework
    * pip install django_filter
    * pip install django-cors-headers


# Ejecutar en localhost
    * cd backend/
    * python manage.py runserver
    * Diríjase a http://127.0.0.1:8000/



## Meta

Daniel Pérez Atanacio - [daniel.pagelab.io/](http://daniel.pagelab.io/) - contact@daniel.pagelab.io
