import uuid
from django.db import models


# Modelo de Contacto
class Contacto(models.Model):
    uuid = models.UUIDField(primary_key=False, editable=False, default=uuid.uuid4)
    nombre = models.CharField(max_length=128)
    apellido_paterno = models.CharField(max_length=128)
    apellido_materno = models.CharField(max_length=128)
    sexo = models.CharField(max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre
