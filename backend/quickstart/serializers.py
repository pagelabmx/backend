from rest_framework import serializers
from quickstart.models import Contacto


class ContactoSerializer(serializers.ModelSerializer):
    custom_uuid = serializers.SerializerMethodField()
    custom_fullname = serializers.SerializerMethodField()

    class Meta:
        model = Contacto
        fields = ('id', 'uuid', 'nombre', 'apellido_paterno', 'apellido_materno', 'sexo', 'created_at', 'custom_uuid', 'custom_fullname')

    @staticmethod
    def get_custom_uuid(obj):
        parseUUID = str(obj.uuid)

        return parseUUID[32:]

    @staticmethod
    def get_custom_fullname(obj):
        return obj.nombre + ' ' + obj.apellido_paterno + ' ' + obj.apellido_materno