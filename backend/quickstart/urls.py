from django.urls import path
from . import views

urlpatterns = [
    path('contact/', views.ContactoList.as_view()),
    path('contact/<int:pk>/', views.ContactDetail.as_view()),
    path('contact/<int:pk>/', views.ContactUpdate.as_view()),
    path('contact/<int:pk>/delete', views.ContactDelete.as_view()),
]