# PYTHON IMPORTS
import json
import ast

from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend

from quickstart.models import Contacto
from quickstart.serializers import ContactoSerializer
from rest_framework import generics


class ContactoList(generics.ListCreateAPIView):
    serializer_class = ContactoSerializer
    queryset = Contacto.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('nombre', 'apellido_paterno', 'apellido_materno')
    ordering_fields = '__all__'
    ordering = ('id',)

    """
        Esta vista retorna una lista de todos los contactos
    """
    def get_queryset(self):
        queryset = Contacto.objects.all()
        filters = self.request.query_params.get('filter', None)

        params = []

        if filters:
            params = json.loads(filters)

        if len(params) > 0:
            search = params['search']

            if params is not None:
                queryset = queryset.filter(Q(nombre__startswith=search)
                                           | Q(apellido_paterno__startswith=search)
                                           | Q(apellido_materno__startswith=search))

        sorting = self.request.query_params.get('sorting', None)

        order = 'ASC'
        field = 'id'

        if sorting:
            sorting_params = ast.literal_eval(sorting)  # Convert string representation of list to list

            field = sorting_params[0]  # nombre
            order = sorting_params[1]  # ASC | DESC

        order_by = ('-' + field) if order == 'DESC' else field

        queryset = queryset.order_by(order_by)

        return queryset


class ContactDetail(generics.RetrieveUpdateAPIView):
    queryset = Contacto.objects.all()
    serializer_class = ContactoSerializer


class ContactUpdate(generics.UpdateAPIView):
    queryset = Contacto.objects.all()
    serializer_class = ContactoSerializer


class ContactDelete(generics.DestroyAPIView):
    queryset = Contacto.objects.all()
    serializer_class = ContactoSerializer
